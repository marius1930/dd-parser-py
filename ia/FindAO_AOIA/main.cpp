#include <windows.h>
#include <Tlhelp32.h>
#include <iostream>
#include <string>
//#define _DEBUG
// GetModuleFilenameEx
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")
using std::wcout;
using std::cout;

int isDebug() {
#ifdef _DEBUG
	int x;
	std::cin >> x;
#endif
	return 0;
}



bool ElevatePriviledges() {
	HANDLE hToken = NULL;
	TOKEN_PRIVILEGES tokenPriv;
	LUID luidDebug;
	bool success = false;
	if(OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE) 
	{
		if(LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidDebug) != FALSE)
		{
			tokenPriv.PrivilegeCount           = 1;
			tokenPriv.Privileges[0].Luid       = luidDebug;
			tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			if(AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, 0, NULL, NULL) != FALSE)
			{
				// Always successful, even in the cases which lead to OpenProcess failure
				cout << "SUCCESSFULLY CHANGED TOKEN PRIVILEGES\n";
				success = true;
			}
			else
			{
				cout << "FAILED TO CHANGE TOKEN PRIVILEGES, CODE: " << GetLastError() << "\n";
				success = false;
			}
		}
	}
	CloseHandle(hToken);
	return success;
}

#ifdef _DEBUG
bool debug = true;
#else
bool debug = false;
#endif

// Usage: 
// No input: Finds AO
// Any Input: Finds AOIA
int main(int argc, char** args) {
	bool aoia = false;
	if (argc > 1) { aoia = true; }
	/*std::wstring processName(L"client.exe");
	if (argc > 1) {
	processName.assign(L"ItemAssistant.exe");
	}*/

	PROCESSENTRY32 entry; entry.dwSize = sizeof(entry);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);      
	if (Process32First(snapshot, &entry) == TRUE){         
		while (Process32Next(snapshot, &entry) == TRUE) {  
			//wcout << _wcslwr(entry.szExeFile) << "\n";


			// Convert to lowercase and compare
			bool cmp;
			if (!aoia)
				cmp = wcscmp(_wcslwr(entry.szExeFile), L"anarchy.exe" ) == 0 || wcscmp(_wcslwr(entry.szExeFile), L"anarchyonline.exe" ) == 0 || wcscmp(_wcslwr(entry.szExeFile), L"client.exe" ) == 0;
			else
				cmp = wcscmp(_wcslwr(entry.szExeFile), L"itemassistant.exe") == 0;

			if (cmp) { 

				HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, entry.th32ProcessID);                  
				if (!hProcess) {
					DWORD e = GetLastError();
					if (e == ERROR_ACCESS_DENIED || e == 5) {
						ElevatePriviledges();
						hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, entry.th32ProcessID);                  
						if (!hProcess) {
							cout << "";
							return isDebug();
						}
					} else {
						if (debug) cout << "Unknown error: " << e << "\n";
						else cout << "-1";
						return isDebug();
					}
				}

				WCHAR fn[MAX_PATH] = {0};
				if (GetModuleFileNameEx(hProcess, 0, fn, MAX_PATH) == 0) {
					cout << "Could not get filename from module handle, Error code: " << GetLastError() << "\n";
					cout << "Handle: "<<hProcess<<" & ID: " << entry.th32ProcessID << "\n";
					cout << "Name detected: "<<entry.szExeFile<< "\n";
				}


				#ifdef _DEBUG
					if (aoia) wcout << "AOIA found: " << fn << L"\n";
					else wcout << "AO found: " << fn << L"\n";
				#else
					wcout << fn << L"";
				#endif

				CloseHandle(hProcess);
				CloseHandle(snapshot);
				return isDebug();
			} 
			#ifdef _DEBUG			
				else {
					wcout << L"Not AO: " << entry.szExeFile << L"\n";
				}
			#endif
		}
	} else {
		cout << "-1";
	}
	CloseHandle(snapshot);
	cout << "-1";
	return isDebug();
}


			