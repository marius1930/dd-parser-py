#include <windows.h>
#include <string>
#include <string>
#include <iostream>
using std::cout;
using std::endl;


extern "C" __declspec(dllexport) bool ElevatePriviledges();

extern "C" __declspec(dllexport) bool InjectDLL(DWORD ProcessID, const char* dllname) //std::string const& dllNameA)
{
	std::string const dllNameA(dllname);
	cout << "[DLL    ] Attempting to inject " << dllNameA << std::endl;



    if(!ProcessID) {
        cout << "[DLL    ] No process ID specified to inject into.";
        return false;
    }

    HANDLE Proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ProcessID);

    if(!Proc) {
		DWORD e = GetLastError();
        cout << "[DLL    ] Could not open process. Error code: " << e << endl;
		if (e == ERROR_ACCESS_DENIED || e == 5) {
			ElevatePriviledges();
			Proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ProcessID);
			if (!Proc) {
				cout << "[DLL    ] Attempt #2: Could not open process. Error code: " << GetLastError() << endl;
				return false;
			}
			else
				cout << "[DLL    ] Attempt #2 succeeded\n";
		}

		else {
			return false; // No recovery, error code != ACCESS_DENIED
		}
        
    }

    LPVOID LoadLibAddy = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
    LPVOID RemoteString = (LPVOID)VirtualAllocEx(Proc, NULL, dllNameA.length(), MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    if (!RemoteString) {
        cout << "[DLL    ] Could not allocate memory in remote process. Error code: " << GetLastError() << endl;
        return false;
    }
    WriteProcessMemory(Proc, (LPVOID)RemoteString, dllNameA.c_str(), dllNameA.length(), NULL);

    if(!CreateRemoteThread(Proc, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibAddy, (LPVOID)RemoteString, NULL, NULL)) {
        DWORD error = GetLastError();
        cout << "[DLL    ] Injection failed with error code: " << error << endl;
    }

    CloseHandle(Proc);

	cout << "[DLL    ] Injection successful.\n";
    return true;
} 
