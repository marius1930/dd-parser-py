#include "dll.h"
#include "parser.h"
#include <iostream>
#include <windows.h>
#include <sstream>
#include <Tlhelp32.h>
#include <vector>
struct LootInfo {
	LootInfo(unsigned int* arr) { lowid = arr[1]; highid = arr[0]; ql = arr[2]; }
	unsigned int lowid;
	unsigned int highid;
	unsigned int ql;
};

// [Server]Parses every highid/lowid/ql from a "open container" message
// Container id returned by reference, 0 if a body
std::vector<LootInfo> parseLootdata(Parser msg, unsigned int& containerID) {
	int i = 0;
	std::vector<LootInfo> output;

	// Ignore 11 bytes
	while (msg.remaining() && i < 11) {
		i++;
		msg.popChar();
	}

	int numItems = (msg.popChar() - 3)/4;
	if (numItems) {
		i = 0;
		while (i < 17 && msg.remaining()) { msg.popChar(); i++; }
	}

	for (int item = 0; item < numItems; item++) {
		i= 0;
		unsigned int data[3];
		while (i < 3 && msg.remaining()) data[i++] = msg.popInteger();
		output.push_back( LootInfo(data) );

		// Remove 20 byte padding behind
		i = 0;
		if (item != numItems-1)
			while (i < 20 && msg.remaining()) { i++; msg.popChar(); }
	}


	containerID = 0;
	while (msg.remaining()) {
		if (msg.popChar() != 199) continue;
		int x = msg.popChar();
		if (x == 73) // Store id only if a backpack
			containerID = msg.popInteger();
		break;
		//if (x == 106)
		//	return output;
		//else if (x == 73) {/*cout << "Backpack! id:"<<id<<endl;*/}
		//else {/*cout << "Unknown? " << x << endl;*/}
	}

	return output;
}


extern "C" __declspec(dllexport) char* StrToChr(const std::string& str, char* test, int len) {
	int target = str.length();
	if (target > len) target = len;
	for (int i = 0; i < target; i++)
		test[i] = str.at(i);
	return test;
}

extern "C" __declspec(dllexport) bool GetLevelID(char* data, unsigned int datasize, unsigned int& out_id, unsigned int& out_level, unsigned int& out_prof, unsigned int& out_charid_self) {
	AOMessageBase msg(data, datasize);
	if (msg.messageId() != SMSG_TBLOB) return false;
	msg.popInteger();
	int level = msg.popChar();
	if (level == 200) return false;		// Can't detect shadowlevels yet, ignore 200.
	int TL = msg.popChar();
	int prof = msg.popChar();
	out_id = msg.entityId();
	out_level = level;
	out_prof = prof;
	out_charid_self = msg.characterId();
	return true;
}

extern "C" __declspec(dllexport) unsigned int getMessageID_s(char* data, unsigned int datasize) {
	AOMessageBase aomsg(data, datasize);
	return aomsg.messageId();
}



// Returns:
// CharID:ContainerID:lowid/highid/ql\n
// One lowid/highid/ql\n entry per item
// For loot: Container ID is 0
// For backpacks: Backpack ID
extern "C" __declspec(dllexport) char* getLootInfo(char* data, unsigned int datasize, char* buf, int buflen) {
	AOMessageBase aomsg(data, datasize);
	if (aomsg.messageId() != 1314089334) return buf; // Not a container message
	std::stringstream st;

	unsigned int containerID;
	std::vector<LootInfo> lootarr = parseLootdata(aomsg, containerID);
	st << aomsg.characterId() << ":" << containerID << ":";

	for (std::vector<LootInfo>::const_iterator it = lootarr.begin(); it != lootarr.end(); it++)
		st << it->lowid << "/" << it->highid << "/" << it->ql << "\n";

	return StrToChr(st.str(), buf, buflen);
}

/*
 Ret: <0 Wrong message
 Ret: 0 Player found
 Ret: 1 Other found
*/
extern "C" __declspec(dllexport) int getPlayerName(char* data, unsigned int datasize, unsigned int& id_out, char* buf, unsigned int bufLen) {
	AOMessageBase msg(data, datasize);
	if (msg.messageId() != MSG_MOB_SYNC) return -1;		// Wrong message
	//if (msg.characterId()!=msg.entityId()) return 1;	// Wrong entity

	MobInfo* pMobInfo = (MobInfo*)msg.start();
	std::string name(&(pMobInfo->characterName.str), pMobInfo->characterName.strLen - 1);
	id_out = msg.entityId();

	StrToChr(name, buf, bufLen);
	return (msg.characterId()==msg.entityId()) ? 0 : 1;
	// return StrToChr(name, buf, bufLen);
}

// Returns: "id1\nid2\nid3\nid4..."
extern "C" __declspec(dllexport) char* GetAOProcID(char* out, int len) {
	// Find the windows
	std::stringstream data;
	std::vector<HWND> clients;
	DWORD pid;
	HWND prevWindow = NULL;
	do {
		prevWindow = FindWindowExW(NULL, prevWindow, L"Anarchy client", NULL);
		if (prevWindow != NULL) {
			clients.push_back(prevWindow);
			GetWindowThreadProcessId( prevWindow, &pid );
			data << pid << "\n";
		}
	} while (prevWindow);


	return StrToChr(data.str(), out, len);
}


BOOL APIENTRY DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved) {
	return TRUE;
}

