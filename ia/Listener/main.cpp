#include "main.h"
#include "dll.h"
using namespace std;
#define _DEBUG
/*
UI.exe update ID TOON


DOES NOT DETECT NEW AO CLIENTS
DOES NOT DETECT AO LOWERCASE

*/
// Initialized from args[]
#define SMSG_MOB_SYNC 0x271B3A6B
#define CMSG_ATTACK_MOB 675889264
#define CMSG_TARGET_MOB 575816799
#define CMSG_LOOT 1381132376
#define CMSG_FLINGSHOT 1363747104
#define CMSG_NCURELATED 1581741936 // could be multi-usage (get 2 of these per jump)
#define SMSG_SUPERSPAM 724778350
#define SMSG_PARTIALSPAM 1174417174
#define SMSG_MOVED 1410404643
#define SMSG_UNKNOWN2 638531185
#define SMSG_UNKNOWN1 2136230149
#define SMSG_TBLOB 1295524910
#define MSG_TYPE_SERVER 1
#define MSG_TYPE_CLIENT 2
#define SMSG_NANO_ME 623988077
#define SMSG_UNKNOWN3 1599226158
#define SMSG_UNKNOWN4 691028809
#define SMSG_UNKNOWN5 1428293414
#define SMSG_UNKNOWN6 1495335206
#define SMSG_UNKNOWN7 490475292			// One every start-attack
#define SMSG_UNKNOWN8 1096961805
#define SMSG_UNKNOWN9 624755264
#define SMSG_UNKNOWN10 1460412473
#define SMSG_ITEMLOOT 1196653092
#define SMSG_CONTAINER 1314089334
#define SMSG_LOOT2 1381132376

#define SMSG_STARTATTACK 675889264 // Receive one on start-attack
#define SMSG_STOPATTACK 1245782078 // Receive 2 on stop attack


std::map<unsigned int, std::string> CharMap;

struct SMSG_STARTATTACK_DATA {
	unsigned int id;
	SMSG_STARTATTACK_DATA(Parser msg) {
		msg.popChar();
		msg.popInteger();
		id = msg.popInteger();
		// 1 byte unread
	}
};
struct CMSG_TARGET_MOB_DATA {
	unsigned int id;
	CMSG_TARGET_MOB_DATA(Parser msg) {
		msg.popChar();
		msg.popInteger(); // unknown
		id = msg.popInteger();
		// 4 bytes remaining
	}
};
struct SMSG_MOVED_DATA {
	char unknown0;
	enum TYPE {
		START_FRONT = 1,		STOP_FRONT = 2, 
		START_BACK = 3,			STOP_BACK = 4, 
		START_SRIGHT = 5,		STOP_SRIGHT = 6, 
		START_SLEFT = 7,		STOP_SLEFT = 8,
		START_ROT_RIGHT = 10,	STOP_ROT_RIGHT = 11, 
		START_ROT_LEFT = 13,	STOP_ROT_LEFT = 14, 
		JUMP = 15, ROTATE_RELATED = 22};
	TYPE state;
	float x,z,y;

	// Angles?
	float unknown_d0;
	float unknown_d1;
	SMSG_MOVED_DATA(AOMessageBase msg) {
		unknown0 = msg.popChar();
		state = (TYPE)msg.popChar();

		// 4*4 bytes unknown ,first row
		float* d;
		int tmp[2] = {0};
		tmp[0] = msg.popInteger(); // 0
		tmp[1] = msg.popInteger(); // float/double? increments on look right, decrements on look left
		d = (float*)&tmp[1]; unknown_d0 = *d;

		tmp[0] = msg.popInteger(); // 0
		tmp[1] = msg.popInteger(); // float/double? increments on look left, decrements on look right
		d = (float*)&tmp[1]; unknown_d1 = *d;

		// Grab coordinates
		float* f;
		int ix = msg.popInteger();
		int iz = msg.popInteger();
		int iy = msg.popInteger();
		f = (float*)&ix; x = *f;
		f = (float*)&iz; z = *f;
		f = (float*)&iy; y = *f;
	}
	std::string getType() {
		switch (state) {
			case START_FRONT: return "START_FORWARDS";
			case STOP_FRONT: return "STOP_FORWARDS";
			case START_BACK: return "START_BACKWARDS";
			case STOP_BACK: return "STOP_BACKWARDS";
			case START_SRIGHT: return "START_STRAFE_RIGHT";
			case STOP_SRIGHT: return "STOP_STRAFE_RIGHT";
			case START_SLEFT: return "START_STRAFE_LEFT";
			case STOP_SLEFT: return "STOP_STRAFE_LEFT";
			case START_ROT_RIGHT: return "START_ROTATE_RIGHT";
			case STOP_ROT_RIGHT: return "STOP_ROTATE_RIGHT";
			case START_ROT_LEFT: return "START_ROTATE_LEFT";
			case STOP_ROT_LEFT: return "STOP_ROTATE_LEFT";
			case JUMP: return "JUMP";
			default: return "Unknown state";
		}
		
	}
};

#ifdef _DEBUG
std::map< std::string, int > gIgnores;
#endif

void intToBinary(char n) {
	int x = 0;
//	for (UINT i = 0x80000000; i >= 1; i /= 2) {
	for (UCHAR i = 128; i >= 1; i /= 2) {
		cout << ((n&i)?'1':'0');

		if (++x == 8) {
			cout << " ";
			x = 0;
		}
	}
}
int TARGET_LEVEL = 25;
void LookForLevel25(AOMessageBase msg) {
	int i = 1;
	while (msg.remaining() > 0) {
		int n = msg.popChar();
		if (n == TARGET_LEVEL) cout << "Found "<<TARGET_LEVEL<<" in byte #"<<i<<"\n";
		//if (n == 200) cout << "Found 200 in byte #"<<i<<"\n";
		i++;
		if (i >= 15) 
			break;
	}
}
void LookForLevel20_220(AOMessageBase msg) {
	int i = 1;
	while (msg.remaining() > 0) {
		int n = msg.popChar();
		if (n == 20) cout << "Found "<<20<<" in byte #"<<i<<"\n";
		if (n == 220) cout << "Found "<<220<<" in byte #"<<i<<"\n";
		//if (n == 200) cout << "Found 200 in byte #"<<i<<"\n";
		i++;
		if (i >= 15) 
			break;
	}
}
void printAllInts_1to20(AOMessageBase msg) {
	int i = 0;
	while (msg.remaining() > 0) {
		int n = msg.popChar();
		if (n > 0 && n < 21) cout << i <<":"<<n << ", ";
		i++;
	} cout << "\n";
}
void printByteX(AOMessageBase msg, int x, std::string Pre) {
	for (int i = 0; i < x-1; i++)
		msg.popChar();
	cout << Pre.c_str() << ": " << (int)msg.popChar() << endl;
}

void dumpBinary(Parser msg) {
	int flip = 0;
	int bc = 0;
	while (msg.remaining() > 0) {
		bc++;
		int n = msg.popChar();
		intToBinary(n);
		flip++;
		if (flip == 8) {
			cout << endl;
			flip = 0;
		}
	}
	cout << "\n";
}

struct LootInfo {
	LootInfo(unsigned int* arr) { lowid = arr[1]; highid = arr[0]; ql = arr[2]; }
	unsigned int lowid;
	unsigned int highid;
	unsigned int ql;
};

struct ObjID {
	unsigned int low, high;
};
std::vector<LootInfo> parseLootdataTEST(AOMessageBase msg) {
	int i = 0;
	std::vector<LootInfo> output;
	msg.characterId();

	// Ignore 11 bytes
	while (msg.remaining() && i < 11) {
		i++;
		msg.popChar();
	}

	int numItems = (msg.popChar() - 3)/4;
//	+17 for first ITEMID_CHILD
//	+20 for next ones

	cout << "Number of items: " << numItems << endl;
	if (numItems) {
		i = 0;
		while (i < 17 && msg.remaining()) { msg.popChar(); i++; }
	}

	for (int item = 0; item < numItems; item++) {
		i= 0;
		unsigned int data[3];
		while (i < 3 && msg.remaining()) data[i++] = msg.popInteger();
		cout << data[1] << "/" << data[0] << "/" << data[2] << endl;

		output.push_back( LootInfo(data) );

		// Remove 20 byte padding behind
		i = 0;
		if (item != numItems-1)
			while (i < 20 && msg.remaining()) { i++; msg.popChar(); }
	}

	//ObjID* containerType = (ObjID*)((char*)msg.start() + numItems*76);
	//if (containerType >= (ObjID*)msg.end()) cout << "Container ptr is " << (char*)containerType << " end is at " << (char*)msg.end() << endl;
	//else cout << "Backpack ID: " << containerType->low << endl;

	bool found = false;
	while (!found && msg.remaining()) {
		if (msg.popChar() != 199) continue;
		int x = msg.popChar();
		unsigned int id = msg.popInteger();
		if (x == 106)
			cout << "Loot, id:"<<id<<endl;
		else if (x == 73)
			cout << "Backpack! id:"<<id<<endl;
		else
			cout << "Unknown? " << x << endl;
		found = true;
	}
	if (!found) cout << "Seems we never found it..\n";

	return output;
}

LRESULT CALLBACK WINAPI stWndProc(HWND hwnd,UINT Wmsg,WPARAM wParam,LPARAM lParam) {
	if (Wmsg == WM_COPYDATA) {
		COPYDATASTRUCT* pData = (COPYDATASTRUCT*)lParam;
		char* datablock = (char*)(pData->lpData);
		unsigned int datasize = pData->cbData;
		char* buf[256] = {0};

		if (pData->dwData == MSG_TYPE_CLIENT) {
			AOClientMessageBase aomsg(datablock, datasize);

			switch (aomsg.messageId()) {
				case CMSG_ATTACK_MOB: cout << "Processing message CLI:MSG_ATTACK_MOB\n"; break;
				case CMSG_TARGET_MOB: {
					CMSG_TARGET_MOB_DATA data(aomsg);
					cout << "CLI:MSG_TARGET_MOB: " << data.id << " (" << CharMap[data.id] << ")\n";  
				} break;
				case CMSG_LOOT: 
					dumpBinary(aomsg);
					cout << "Processing message CLI:MSG_LOOT\n"; 
					break;
				//case CMSG_FLINGSHOT: cout << "Processing message CLI:MSG_FLINGSHOT\n"; break;
				//case CMSG_NCURELATED: cout << "Processing message CLI:MSG_NCURELATED\n"; break;

				case SMSG_CONTAINER:
					cout << "Client container!\n";
					break;

				default:
					cout << "CLI:Unknown: " << aomsg.messageId() << endl;
					dumpBinary(aomsg);
					break;
			}
			
			/*
			if (aomsg.messageId() == MSG_TARGET_MOB) {
				cout << "Data:\n";
				int flip = 0;
				int bc = 0;
				while (aomsg.remaining() > 0) {
					bc++;
					int n = aomsg.popChar();
					intToBinary(n);
					flip++;
					if (flip == 8) {
						cout << endl;
						flip = 0;
					}
				}
				cout << "\n";*/
		}
		if (pData->dwData == MSG_TYPE_SERVER) {
			AOMessageBase aomsg(datablock, datasize);

			
			switch (aomsg.messageId()) {
				case SMSG_MOB_SYNC: {	
					MobInfo* pMobInfo = (MobInfo*)aomsg.start();
					std::string name(&(pMobInfo->characterName.str), pMobInfo->characterName.strLen - 1);
					UINT id = aomsg.entityId();
					UINT sid = aomsg.characterId();
					//if ( id == sid ) cout << "MSG_MOB_SYNC (me): " << id << " mapped to '" << name.c_str() << "'\n";
					//else cout << "MSG_MOB_SYNC: " << id << " mapped to '" << name.c_str() << "'\n";
					CharMap[id] = name;
					//cout << "Processing message MSG_MOB_SYNC (sets names to ids)\n"; 
				} break;


				/*case SMSG_NANO_ME: {
						cout << "Processing SMSG_NANO_ME ";
						dumpBinary(aomsg);
						aomsg.popChar();
						int nanoID = aomsg.popInteger();
						cout << "Nano id: " << nanoID << endl;
					} break;

				case 1347702041:
					cout << "MSG: 1347702041 -- Dump:\n";
					dumpBinary(aomsg);
					break;
				
				case SMSG_MOVED: {
					//SMSG_MOVED_DATA md(aomsg);
					//cout << "Processing SMSG_MOVED ("<<md.x<<", "<<md.y<<", "<<md.z<<") ("<<md.unknown_d0<<", "<<md.unknown_d1<<") : "<<md.getType() << endl;
						//dumpBinary(aomsg);
					} break;
				
				case SMSG_UNKNOWN1: 
					cout << "Processing message MSG_UNKNOWN1\n";
					break;

				case SMSG_PARTIALSPAM: 
					cout << "Processing message SMSG_PARTIALSPAM\n";
					break;

					// ticks every 500 ms after i debuffed myself
				case SMSG_SUPERSPAM:  
					//cout << "Processing message SMSG_SUPERSPAM\n";
					break;

				// Spams every 500ms or so
				case SMSG_UNKNOWN2: 
					//cout << "Processing message SMSG_UNKNOWN2\n";
					break;
*/

				//case SMSG_UNKNOWN3: { cout << "SMSG_UNKNOWN3\n"; dumpBinary(aomsg); } break;
				//case SMSG_UNKNOWN4: { cout << "SMSG_UNKNOWN4\n"; dumpBinary(aomsg); } break;  // freaking huge
				//case SMSG_UNKNOWN5:  { cout << "SMSG_UNKNOWN5\n"; dumpBinary(aomsg); } break;
				//case SMSG_UNKNOWN6:  { cout << "SMSG_UNKNOWN6\n"; dumpBinary(aomsg); } break;
				//case SMSG_UNKNOWN7:  { cout << "SMSG_UNKNOWN7\n"; dumpBinary(aomsg); } break;
				//case SMSG_UNKNOWN8:  { cout << "SMSG_UNKNOWN8\n"; dumpBinary(aomsg); } break;
				//case SMSG_UNKNOWN9:  { cout << "SMSG_UNKNOWN9\n"; dumpBinary(aomsg); } break; // fucking enormous
				//case SMSG_UNKNOWN10:  { cout << "SMSG_UNKNOWN10\n"; dumpBinary(aomsg); } break; // 1 byte data
				case SMSG_ITEMLOOT: 
					{ /* to_to_to = backpack ID of destination
						00000000 00000000 00000000 00000000 01101011 00000000 01101100 00000000
						00000001 00000000 00000000 11000111 01001001 to_to_to to_to_to to_to_to
						to_to_to 00000000 00000000 00000000 01101111 */ 
						cout << "SMSG_ITEMLOOT\n"; 
						dumpBinary(aomsg); 
					} break;
				case SMSG_CONTAINER: { 
					cout << "SMSG_LOOT1\n"; parseLootdataTEST(aomsg); dumpBinary(aomsg); 
				}  break;
				//case SMSG_LOOT2: { cout << "SMSG_LOOT2\n"; dumpBinary(aomsg); } break;

				//case SMSG_STARTATTACK: { 
				//	SMSG_STARTATTACK_DATA d(aomsg);
				//	cout << "SMSG_START_ATTACK (" << d.id << ", " << CharMap[d.id] << ")\n";// dumpBinary(aomsg); 
				//} break;


				case SMSG_STOPATTACK: { 
					// Data: 1 byte unknown, 1 integer == 1
					cout << "SMSG_STOP_ATTACK {someone is hit?}\n"; //dumpBinary(aomsg); 
				} break;

				case SMSG_TBLOB:{

					cout << "Processing message SMSG_TBLOB\n";
					cout << "Entity: " << aomsg.entityId() << endl;
					//printByteX(aomsg, 5, "Level: "); 
					//printByteX(aomsg, 6, "TL: "); 
					//printByteX(aomsg, 7, "Prof: "); 
					/*
					//LookForLevel25(aomsg); 
					//LookForLevel20_220(aomsg); 
					int flip = 0;
					int bc = 0;
					while (aomsg.remaining() > 0) {
						bc++;
						int n = aomsg.popChar();
						intToBinary(n);
						flip++;
						if (flip == 8) {
							cout << endl;
							flip = 0;
						}
					}
					cout << "\n";*/

					
					{/*
						AOMessageBase dup = aomsg;
						printAllInts(dup);
						dup.popChar(); printAllInts(dup);
						dup.popChar(); printAllInts(dup);
						dup.popChar(); printAllInts(dup);*/
					}
					
					
								}break;
				
				default: 
					//cout << "Processing message " << aomsg.messageId() << "\n"; LookForLevel25(aomsg);
					break;
			}

			unsigned int id, level, prof, sid;
			if (GetLevelID(datablock, datasize, id, level, prof, sid))
				cout << "Got level for ID:"<<id<<" level:"<<level<<endl;
			
		
			/*
			if (aomsg.messageId() == MSG_MOB_SYNC) {


				cout << "Data:\n";
				int flip = 0;
				int bc = 0;
				while (aomsg.remaining() > 0) {
					bc++;
					int n = aomsg.popChar();
					intToBinary(n);
					//cout << " ";
					flip++;
					if (flip == 8) {
						cout << endl;
						flip = 0;
					}
				}
				cout << "\n";
			}

*/
			int ip = getPlayerName(datablock, datasize, id, (char*)buf, 256);
			if (ip == 0) {
				int id = -1;
				
				std::string name((char*)buf)/* = getPlayerName(datablock, datasize, id, (char*)buf, 256)*/;
				
				addToMap(id, name);
				cout << "Player found: " << name.c_str() << "   <------\n";
			}
			else if (ip == 1) {
				int id = -1;
#ifdef _DEBUG	
				std::string name( (char*) buf);//getPlayerName(datablock, datasize, id, (char*)buf, 256);
				
				if (gIgnores.find(name) == gIgnores.end()) {
					cout << "NotMe: " << name.c_str() << endl;
					gIgnores[name] = 0;
				}
#endif
			}
		}

	}
	return DefWindowProc(hwnd,Wmsg,wParam,lParam);
}













// Returns false if UI.exe is not running
bool gEndProgram = false;
bool keepRunning() {
	return !GetProcID("ui.exe").empty();
}

std::map<int, int> injectedPids;
void findAndInject() {
	// Prepare dll path
	char CurrDir[MAX_PATH];
    GetCurrentDirectory( MAX_PATH, CurrDir );
	string str = string(CurrDir);
	str.append("\\custom.dll");
	cout << str.c_str() << endl;

	// Inject dll into every AO process
	std::vector<DWORD> idlist = GetProcID("client.exe");
	std::vector<DWORD> idlist2 = GetProcID("anarchyonline.exe");
	for (std::vector<DWORD>::iterator it = idlist2.begin(); it != idlist2.end(); it++)
		idlist.push_back(*it);

	for (int i = 0; i < idlist.size(); i++) {
		/*if (idlist[i] != 0xBE0) {
			cout << "Skipping inject on id "<<idlist[i]<<endl;
			continue;
		}*/
		if (injectedPids.find(idlist[i]) != injectedPids.end()) continue; // Skip any previously injected pids
		cout << "Injecting into pid "<<idlist[i]<<endl;
		if (InjectDLL(idlist[i], str.c_str()))
			injectedPids[idlist[i]] = 0;			// Mark as injected
	}

	cout << "THIS PROBABLY DID NOT WORK!! RUN THE DAMAGE PARSER TO INJECT!\n";
}


VOID CALLBACK TimerProc(  __in  HWND hwnd,  __in  UINT uMsg,  __in  UINT_PTR idEvent,  __in  DWORD dwTime  ) {
	if (!keepRunning()) {
		gEndProgram = true;
	}
	else
		findAndInject();
}
#ifdef _DEBUG
int main() {
HINSTANCE hInstance = (HINSTANCE)GetModuleHandle(NULL);
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int) {
#endif


	// Make sure there's only one instance running
	HANDLE mutex = CreateMutex(NULL, 1, "DcdumpUniqueMutex");
	if (!mutex || GetLastError() == ERROR_ALREADY_EXISTS) {
		cout << "Process already running..\n";
		return 0;
	}
	WaitForSingleObject(mutex, 0);

	// Grab input IDs for toons to ignore
	for (int i = 1; i < __argc; i++)
		addToMapIgnore(atoi(__argv[i]));
	


	if (!regClass(hInstance)) {
		cout << "Failed registering listener class\n";
		return 1;
	}



	// Create receiving window
	HWND receiver = CreateWindow("DCDumpLogger", "DCDumpLogger", 0, 0, 0, 0, 0, HWND_MESSAGE, 0, GetModuleHandle(0), 0);
	if (receiver == 0) {
		cout << "Failed to create receiver window\n";
		return 1;
	}




	{ // Allow for messages from lower to higher.. bla bla..
		typedef  BOOL (WINAPI *ChangeWindowMessageFilterFunc)(UINT message, DWORD dwFlag);
        ChangeWindowMessageFilterFunc f = (ChangeWindowMessageFilterFunc)GetProcAddress(LoadLibrary("user32.dll"), "ChangeWindowMessageFilter");
        if (f) {
            (f)(WM_COPYDATA, MSGFLT_ADD);
        }
    }








	// Inject into any running AO client
	findAndInject();

	// Set timer for finding new AO clients and detecting parser quit
	SetTimer(receiver, 1000, 1000*10, TimerProc);

	MSG msg = {0};
	while( GetMessage(&msg, 0, 0, 0) ) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (msg.message == WM_QUIT) break;
		if (gEndProgram) {
			
#ifndef _DEBUG
			cout << "NO parser, quitting\n";
			break;
#else
			//cout << "No parser detected.. <supposed to terminate here>\n";
#endif
		}
	}

	// Close it for neatness
	CloseHandle(mutex);
	return 0;
}