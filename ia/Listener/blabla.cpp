#include "main.h"
using namespace std;
map<UINT, std::string> toonMap;
void addToMap(UINT id, std::string toon) {
	// If in map: Ignore
	if (toonMap.find(id) != toonMap.end()) return;
	toonMap[id] = toon;
	cout << "Got id="<<id<<" for toon "<<toon.c_str()<<endl;
	
	{
		char CurrDir[MAX_PATH], args[MAX_PATH] = {0};
		GetCurrentDirectory( MAX_PATH, CurrDir );
		sprintf(args, "UI.exe update %u %s", id, toon.c_str());
		cout << "Executing: " << args << endl;
		PROCESS_INFORMATION trash;
		STARTUPINFO trash2 = {0};
		CreateProcess("UI.exe", args, 0, 0, 0, DETACHED_PROCESS, 0, CurrDir, &trash2, &trash);
	}
}

void addToMapIgnore(UINT id) {
	toonMap[id] = "Ignored";
}






bool regClass(HINSTANCE hInstance) {
	WNDCLASSEX wnd;
	wnd.cbSize				= sizeof(WNDCLASSEX);
	wnd.cbClsExtra			= 0;
	wnd.cbWndExtra			= 0;
	wnd.hbrBackground		= (HBRUSH) GetStockObject(WHITE_BRUSH);
	wnd.hCursor				= LoadCursor(NULL,IDC_ARROW);
	wnd.hIcon				= LoadIcon(NULL,IDI_WINLOGO);
	wnd.hIconSm				= LoadIcon(NULL,IDI_WINLOGO);
	wnd.hInstance			= hInstance;
	wnd.lpfnWndProc			= stWndProc;
	wnd.lpszClassName		= "DCDumpLogger";
	wnd.lpszMenuName		= 0;
	wnd.style				= CS_VREDRAW | CS_HREDRAW;

	if (!RegisterClassEx(&wnd))
		return false;
	return true;
}








std::vector<DWORD> GetProcID(string ProcName) {
	std::vector<DWORD> out;
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	//int nListAmount;
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	pe32.dwSize = sizeof(PROCESSENTRY32); 
	do{
		

		// Consider calling Module32First() to detect if this is the right ao-exe (??)
		if(  _strlwr(pe32.szExeFile) == ProcName ){
			DWORD ProcId = pe32.th32ProcessID;
			out.push_back(ProcId);
		}
	} while(Process32Next(hProcessSnap, &pe32));
	CloseHandle(hProcessSnap);
	return out;
}




extern "C" __declspec(dllexport) bool ElevatePriviledges() {
	HANDLE hToken = NULL;
	TOKEN_PRIVILEGES tokenPriv;
	LUID luidDebug;
	bool success = false;
	if(OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE) 
	{
		if(LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidDebug) != FALSE)
		{
			tokenPriv.PrivilegeCount           = 1;
			tokenPriv.Privileges[0].Luid       = luidDebug;
			tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			if(AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, 0, NULL, NULL) != FALSE)
			{
				// Always successful, even in the cases which lead to OpenProcess failure
				cout << "SUCCESSFULLY CHANGED TOKEN PRIVILEGES" << endl;
				success = true;
			}
			else
			{
				cout << "FAILED TO CHANGE TOKEN PRIVILEGES, CODE: " << GetLastError() << endl;
				success = false;
			}
		}
	}
	CloseHandle(hToken);
	return success;
}