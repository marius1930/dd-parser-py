#include "Parser.h"

Parser::Parser(char* memory, unsigned int size)
    : m_start(memory)
    , m_end(memory + size)
    , m_pos(memory)
{
}


Parser::~Parser()
{
}


unsigned int Parser::remaining() const
{
    if (m_end > m_start) {
        return (unsigned int)(m_end - m_pos);
    }
    return 0;
}


void Parser::skip( unsigned int count )
{
//    assert(count <= remaining());

    if (count <= remaining()) {
        m_pos += count;
    }
}


unsigned char Parser::popChar() const
{
    return *(m_pos++);
}


unsigned short Parser::popShort() const
{
    unsigned short retval = 0;
    memcpy(&retval, m_pos, 2);
    m_pos += 2;
    return _byteswap_ushort(retval);
}


unsigned int Parser::popInteger() const
{
    unsigned int retval = 0;
    memcpy(&retval, m_pos, 4);
    m_pos += 4;
    return _byteswap_ulong(retval);
}


std::string Parser::popString() const
{
    unsigned short len = popChar();
    std::string retval(m_pos, len);
    m_pos += len + 1;
    return retval;
}


unsigned int Parser::pop3F1Count() const
{
    unsigned int val = popInteger();
    return (val / 1009) - 1;
}



    AOMessageBase::AOMessageBase(char *pRaw, unsigned int size)
        : Parser(pRaw, size) 
        , m_type(UNKNOWN_MESSAGE)
        , m_size(0)
        , m_characterid(0)
        , m_messageid(0)
        , m_entityid(0)
    {
        skip(2);    // Skip the sequence number (short). introduced in v18.1 and replaces the old 0xDFDF sequence.

        // Parse and validate header
        unsigned short t = popShort();
        if (t == 0x000A) {
            m_type = MSG_TYPE_A;
        }
        else if (t == 0x000D) {
            m_type = MSG_TYPE_D;
            char c = popChar();
            //assert(c == 0x0A);
        }
        else if (t == 0x000B) {
            m_type = MSG_TYPE_B;
        }
        else if (t == 0x000E) {
            m_type = MSG_TYPE_E;
        }
        else if (t == MSG_TYPE_1) {
            m_type = MSG_TYPE_1;
        }
        else {
            //Logger::instance().log(STREAM2STR(_T("Error unknown message header: ") << t));
            return;
        }

        skip(2);    // 0x0001		protocol version?
        m_size = popShort();
        skip(4);    // Receiver: Instance ID? Dimension? Subsystem?
        m_characterid = popInteger();
        m_messageid = popInteger();
        skip(4);    // Target Entity: Instance ID? Dimension? Subsystem?
        m_entityid = popInteger();
    }


	
    AOClientMessageBase::AOClientMessageBase(char *pRaw, unsigned int size)
        : Parser(pRaw, size) 
        , m_type(UNKNOWN_MESSAGE)
        , m_characterid(0)
        , m_messageid(0)
    {
        //Client Send Header parsing:

        //H3 = I4 msgType  I4 Zero     I4  someId  I4   02     I4 msgId I4+I4 charId
        // Parse and validate header
        unsigned int t = popInteger();
        if (t == 0x0000000A) {
            m_type = MSG_TYPE_A;
        }
        //else if (t == 0x0000000D) {
        //    m_type = MSG_TYPE_D;
        //   // char c = popChar();
        //   // assert(c == 0x0A);
        //}
        else if (t == 0x0000000B) {
            m_type = MSG_TYPE_B;
        }
        //else if (t == 0x0000000E) {
        //    m_type = MSG_TYPE_E;
        //}
        else if (t == 0x00000001) {
            m_type = MSG_TYPE_1;
        }
        else {
            //Logger::instance().log(STREAM2STR(_T("Error unknown client message header: ") << t));
            return;
        }

        skip(4);    // 0x00 00 00 00
        skip(4);    // Receiver: Instance ID? Dimension? Subsystem?
        skip(4); //??ex 00 00 00 02 or 00 00 0b ed  on logoff

        m_messageid = popInteger();

        skip(4);    // Target Entity: Instance ID? Dimension? Subsystem? 50000
        //m_entityid = popInteger();

        m_characterid = popInteger();
    }