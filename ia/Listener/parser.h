#include <string>
class Parser
{
public:
    Parser(char* memory, unsigned int size);
    ~Parser();

    // Number of bytes remaining to be parsed
    unsigned int remaining() const;

    // Moves the current position the number of bytes ahead
    void skip(unsigned int count);

    // Get the next byte
    unsigned char popChar() const;

    // Get the next 2 bytes 
    unsigned short popShort() const;

    // Get the next 4 bytes
    unsigned int popInteger() const;

    // Get the string at current position. <len><char[]><NULL>
    std::string popString() const;

    // Get the 3F1 encoded counter value 
    // counter = (x/1009)-1
    unsigned int pop3F1Count() const;

    char* start() const { return m_start; }
    char* end() const { return m_end; }
    char* pos() const { return m_pos; }

private:
    char* m_start;
    char* m_end;
    mutable char* m_pos;
};
class AOMessageBase : public Parser
    {
    public:
        enum HeaderType {
            UNKNOWN_MESSAGE,
            MSG_TYPE_1 = 0x0001,
            MSG_TYPE_A = 0x000A,
            MSG_TYPE_B = 0x000B,
            MSG_TYPE_D = 0x000D,
            MSG_TYPE_E = 0x000E,
        };

        AOMessageBase(char *pRaw, unsigned int size);

        HeaderType headerType() const { return m_type; }
        unsigned int size() const { return m_size; }
        unsigned int characterId() const { return m_characterid; }
        unsigned int messageId() const { return m_messageid; }
        unsigned int entityId() const { return m_entityid; }

    private:
        HeaderType m_type;
        unsigned int m_size;
        unsigned int m_characterid;
        unsigned int m_messageid;
        unsigned int m_entityid;
    };

class AOClientMessageBase
        : public Parser
    {
    public:
        enum HeaderType {
            UNKNOWN_MESSAGE,
            MSG_TYPE_1 = 0x00000001,
            MSG_TYPE_A = 0x0000000A,
            MSG_TYPE_B = 0x0000000B,
            MSG_TYPE_D = 0x0000000D,
            MSG_TYPE_E = 0x0000000E,
        };

        AOClientMessageBase(char *pRaw, unsigned int size);

        HeaderType headerType() const { return m_type; }
        unsigned int characterId() const { return m_characterid; }
        unsigned int messageId() const { return m_messageid; }

    private:
        HeaderType m_type;
        unsigned int m_characterid;
        unsigned int m_messageid;
    };


    struct AoObjectId {
        unsigned int low;
        unsigned int high;
    };
    struct AoString {
        unsigned char   strLen;         // Number of bytes allocated to 'str'
        const char      str;            // array of strLen number of bytes.
    };
	   struct Header {
        unsigned int	headerid;	// 0xDFDF000A
        unsigned short	unknown1;	// 0x0001		protocol version?
        unsigned short	msgsize;	//				size incl header
        unsigned int	serverid;	// 0x00000C20	ServerID
        unsigned int	charid;		// 0x1F26E77B	== Adjuster
        unsigned int	msgid;		//
        AoObjectId		target;		// 0x0000C350	dimension? (50000:<charid>)
    };
    struct MobInfo
    {
        Header         header;
        char           unknown[42];
        AoString       characterName;
    };