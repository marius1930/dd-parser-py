#include "InjectionSupport.h"
#include <vector>
#include <iostream>
#include <string>
#include "parser.h"
#include <windows.h>
#include <stdio.h> 
#include <map>
#include <Tlhelp32.h>
void addToMap(UINT id, std::string toon);
void addToMapIgnore(UINT id);
bool regClass(HINSTANCE hInstance);
std::vector<DWORD> GetProcID(std::string ProcName);
LRESULT CALLBACK WINAPI stWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
bool keepRunning();
extern "C" __declspec(dllexport) bool ElevatePriviledges();