#ifndef DLL_H
#define DLL_H

#define MSG_MOB_SYNC 0x271B3A6B
#define SMSG_TBLOB 1295524910
//extern "C" __declspec(dllexport) int isPlayer(char* data, int datasize);
extern "C" __declspec(dllexport) int getPlayerName(char* data, unsigned int datasize, unsigned int& id_out, char* buf, unsigned int bufLen);
extern "C" __declspec(dllexport) char* GetProcID(char* ProcName, char* out, int len);
extern "C" __declspec(dllexport) bool GetLevelID(char* data, unsigned int datasize, unsigned int& out_id, unsigned int& out_level, unsigned int& out_prof, unsigned int& out_charid_self);
extern "C" __declspec(dllexport) unsigned int getMessageID(char* data, unsigned int datasize);

#endif